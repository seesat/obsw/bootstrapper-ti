/*
 * System.h
 *
 *     Project: AssemblerTestProjekt
 *  Created on: 25.11.2023
 *      Author: Pascal Fiedler
 *    Function: Definition of various System Variables
 */

#ifndef INCLUDE_SYSTEM_H_
#define INCLUDE_SYSTEM_H_

#include "Errata_SSWF021_45_Defs.h"




/* Flash Register Frame Definition */
/** @struct flashWBase
*   @brief Flash Wrapper Register Frame Definition
*
*   This type is used to access the Flash Wrapper Registers.
*/
/** @typedef flashWBASE_t
*   @brief Flash Wrapper Register Frame Type Definition
*
*   This type is used to access the Flash Wrapper Registers.
*/
typedef volatile struct flashWBase
{
    uint32 FRDCNTL;         /* 0x0000 */
    uint32 rsvd1;           /* 0x0004 */
    uint32 EE_FEDACCTRL1;   /* 0x0008 */
    uint32 rsvd2;           /* 0x000C */
    uint32 rsvd3;           /* 0x0010 */
    uint32 FEDAC_PASTATUS;  /* 0x0014 */
    uint32 FEDAC_PBSTATUS;  /* 0x0018 */
    uint32 FEDAC_GBLSTATUS; /* 0x001C */
    uint32 rsvd4;           /* 0x0020 */
    uint32 FEDACSDIS;       /* 0x0024 */
    uint32 FPRIM_ADD_TAG;   /* 0x0028 */
    uint32 FDUP_ADD_TAG;    /* 0x002C */
    uint32 FBPROT;          /* 0x0030 */
    uint32 FBSE;            /* 0x0034 */
    uint32 FBBUSY;          /* 0x0038 */
    uint32 FBAC;            /* 0x003C */
    uint32 FBPWRMODE;       /* 0x0040 */
    uint32 FBPRDY;          /* 0x0044 */
    uint32 FPAC1;           /* 0x0048 */
    uint32 rsvd5;           /* 0x004C */
    uint32 FMAC;            /* 0x0050 */
    uint32 FMSTAT;          /* 0x0054 */
    uint32 FEMU_DMSW;       /* 0x0058 */
    uint32 FEMU_DLSW;       /* 0x005C */
    uint32 FEMU_ECC;        /* 0x0060 */
    uint32 FLOCK;           /* 0x0064 */
    uint32 rsvd6;           /* 0x0068 */
    uint32 FDIAGCTRL;       /* 0x006C */
    uint32 rsvd7;           /* 0x0070 */
    uint32 FRAW_ADDR;       /* 0x0074 */
    uint32 rsvd8;           /* 0x0078 */
    uint32 FPAR_OVR;        /* 0x007C */
    uint32 rsvd9[13U];      /* 0x0080 - 0x00B0 */
    uint32 RCR_VALID;       /* 0x00B4 */
    uint32 ACC_THRESHOLD;   /* 0x00B8 */
    uint32 rsvd10;          /* 0x00BC */
    uint32 FEDACSDIS2;      /* 0x00C0 */
    uint32 rsvd11;          /* 0x00C4 */
    uint32 rsvd12;          /* 0x00C8 */
    uint32 rsvd13;          /* 0x00CC */
    uint32 RCR_VALUE0;      /* 0x00D0 */
    uint32 RCR_VALUE1;      /* 0x00D4 */
    uint32 rsvd14[108U];    /* 0x00D8 - 0x00284 */
    uint32 FSM_WR_ENA;      /* 0x0288 */
    uint32 rsvd15[11U];     /* 0x028C - 0x002B4 */
    uint32 EEPROM_CONFIG;   /* 0x02B8 */
    uint32 rsvd16;          /* 0x02BC */
    uint32 FSM_SECTOR1;     /* 0x02C0 */
    uint32 FSM_SECTOR2;     /* 0x02C4 */
    uint32 rsvd17[78U];     /* 0x02A8 */
    uint32 FCFG_BANK;       /* 0x02B8 */

} flashWBASE_t;

/** @def flashWREG
*   @brief Flash Wrapper Register Frame Pointer
*
*   This pointer is used by the system driver to access the flash wrapper registers.
*/
#define flashWREG ((flashWBASE_t *)(0xFFF87000U))

/** @enum flashWPowerModes
*   @brief Alias names for flash bank power modes
*
*   This enumeration is used to provide alias names for the flash bank power modes:
*     - sleep
*     - standby
*     - active
*/
enum flashWPowerModes
{
    SYS_SLEEP   = 0U, /**< Alias for flash bank power mode sleep   */
    SYS_STANDBY = 1U, /**< Alias for flash bank power mode standby */
    SYS_ACTIVE  = 3U  /**< Alias for flash bank power mode active  */
};


/** @enum systemClockSource
*   @brief Alias names for clock sources
*
*   This enumeration is used to provide alias names for the clock sources:
*     - Oscillator
*     - Pll1
*     - External1
*     - Low Power Oscillator Low
*     - Low Power Oscillator High
*     - PLL2
*     - External2
*     - Synchronous VCLK1
*/
enum systemClockSource
{
    SYS_OSC             = 0x0U,  /**< Alias for oscillator clock Source                */
    SYS_PLL1            = 0x1U,  /**< Alias for Pll1 clock Source                      */
    SYS_EXTERNAL1       = 0x3U,  /**< Alias for external clock Source                  */
    SYS_LPO_LOW         = 0x4U,  /**< Alias for low power oscillator low clock Source  */
    SYS_LPO_HIGH        = 0x5U,  /**< Alias for low power oscillator high clock Source */
    SYS_PLL2            = 0x6U,  /**< Alias for Pll2 clock Source                      */
    SYS_EXTERNAL2       = 0x7U,  /**< Alias for external 2 clock Source                */
    SYS_VCLK            = 0x9U,  /**< Alias for synchronous VCLK1 clock Source         */
    SYS_PLL2_ODCLK_8    = 0xEU,  /**< Alias for PLL2_post_ODCLK/8                      */
    SYS_PLL2_ODCLK_16   = 0xFU   /**< Alias for PLL2_post_ODCLK/8                      */
};


/** @enum resetSource
*   @brief Alias names for reset sources
*
*   This enumeration is used to provide alias names for the reset sources:
*     - Power On Reset
*     - Osc Failure Reset
*     - Watch Dog Reset
*     - Icepick Reset
*     - CPU Reset
*     - Software Reset
*     - External Reset
*
*/
typedef enum
{
    POWERON_RESET       = 0x8000U,  /**< Alias for Power On Reset     */
    OSC_FAILURE_RESET   = 0x4000U,  /**< Alias for Osc Failure Reset  */
    WATCHDOG_RESET      = 0x2000U,  /**< Alias for Watch Dog Reset    */
    WATCHDOG2_RESET     = 0x1000U,  /**< Alias for Watch Dog 2 Reset  */
    DEBUG_RESET         = 0x0800U,  /**< Alias for Debug Reset        */
    INTERCONNECT_RESET  = 0x0080U,  /**< Alias for Interconnect Reset */
    CPU0_RESET          = 0x0020U,  /**< Alias for CPU 0 Reset        */
    SW_RESET            = 0x0010U,  /**< Alias for Software Reset     */
    EXT_RESET           = 0x0008U,  /**< Alias for External Reset     */
    NO_RESET            = 0x0000U   /**< Alias for No Reset           */
}resetSource_t;


/** @enum config_value_type
*   @brief config type definition
*/
/** @typedef config_value_type_t
*   @brief config type Type Definition
*
*   This type is used to specify the Initial and Current value.
*/
typedef enum config_value_type
{
    InitialValue,
    CurrentValue
}config_value_type_t;

/* Configuration registers */
typedef struct system_config_reg
{
    uint32 CONFIG_SYSPC1;
    uint32 CONFIG_SYSPC2;
    uint32 CONFIG_SYSPC7;
    uint32 CONFIG_SYSPC8;
    uint32 CONFIG_SYSPC9;
    uint32 CONFIG_CSDIS;
    uint32 CONFIG_CDDIS;
    uint32 CONFIG_GHVSRC;
    uint32 CONFIG_VCLKASRC;
    uint32 CONFIG_RCLKSRC;
    uint32 CONFIG_MSTGCR;
    uint32 CONFIG_MINITGCR;
    uint32 CONFIG_MSINENA;
    uint32 CONFIG_PLLCTL1;
    uint32 CONFIG_PLLCTL2;
    uint32 CONFIG_SYSPC10;
    uint32 CONFIG_LPOMONCTL;
    uint32 CONFIG_CLKTEST;
    uint32 CONFIG_DFTCTRLREG1;
    uint32 CONFIG_DFTCTRLREG2;
    uint32 CONFIG_GPREG1;
    uint32 CONFIG_RAMGCR;
    uint32 CONFIG_BMMCR1;
    uint32 CONFIG_CLKCNTL;
    uint32 CONFIG_ECPCNTL;
    uint32 CONFIG_DEVCR1;
    uint32 CONFIG_SYSECR;
    uint32 CONFIG_PLLCTL3;
    uint32 CONFIG_STCCLKDIV;
    uint32 CONFIG_ECPCNTL1;
    uint32 CONFIG_CLK2CNTRL;
    uint32 CONFIG_VCLKACON1;
    uint32 CONFIG_HCLKCNTL;
    uint32 CONFIG_CLKSLIP;
    uint32 CONFIG_EFC_CTLEN;
} system_config_reg_t;


/* Pcr Register Frame Definition */
/** @struct pcrBase
*   @brief Pcr Register Frame Definition
*
*   This type is used to access the Pcr Registers.
*/
/** @typedef pcrBASE_t
*   @brief PCR Register Frame Type Definition
*
*   This type is used to access the PCR Registers.
*/
typedef volatile struct pcrBase
{
    uint32 PMPROTSET0;    /* 0x0000 */
    uint32 PMPROTSET1;    /* 0x0004 */
    uint32 rsvd1[2U];     /* 0x0008 */
    uint32 PMPROTCLR0;    /* 0x0010 */
    uint32 PMPROTCLR1;    /* 0x0014 */
    uint32 rsvd2[2U];     /* 0x0018 */
    uint32 PPROTSET0;     /* 0x0020 */
    uint32 PPROTSET1;     /* 0x0024 */
    uint32 PPROTSET2;     /* 0x0028 */
    uint32 PPROTSET3;     /* 0x002C */
    uint32 rsvd3[4U];     /* 0x0030 */
    uint32 PPROTCLR0;     /* 0x0040 */
    uint32 PPROTCLR1;     /* 0x0044 */
    uint32 PPROTCLR2;     /* 0x0048 */
    uint32 PPROTCLR3;     /* 0x004C */
    uint32 rsvd4[4U];     /* 0x0050 */
    uint32 PCSPWRDWNSET0; /* 0x0060 */
    uint32 PCSPWRDWNSET1; /* 0x0064 */
    uint32 rsvd5[2U];     /* 0x0068 */
    uint32 PCSPWRDWNCLR0; /* 0x0070 */
    uint32 PCSPWRDWNCLR1; /* 0x0074 */
    uint32 rsvd6[2U];     /* 0x0078 */
    uint32 PSPWRDWNSET0;  /* 0x0080 */
    uint32 PSPWRDWNSET1;  /* 0x0084 */
    uint32 PSPWRDWNSET2;  /* 0x0088 */
    uint32 PSPWRDWNSET3;  /* 0x008C */
    uint32 rsvd7[4U];     /* 0x0090 */
    uint32 PSPWRDWNCLR0;  /* 0x00A0 */
    uint32 PSPWRDWNCLR1;  /* 0x00A4 */
    uint32 PSPWRDWNCLR2;  /* 0x00A8 */
    uint32 PSPWRDWNCLR3;  /* 0x00AC */
    uint32 rsvd8[4U];     /* 0x00B0 */
    uint32 PDPWRDWNSET;   /* 0x00C0 */
    uint32 PDPWRDWNCLR;   /* 0x00C4 */
    uint32 rsvd9[78U];    /* 0x00C8 */
    uint32 MSTIDWRENA;    /* 0x0200 */
    uint32 MSTIDENA;      /* 0x0204 */
    uint32 MSTIDDIAGCTRL; /* 0x0208 */
    uint32 rsvd10[61U];   /* 0x020C */
    struct
    {
        uint32 PSxMSTID_L;
        uint32 PSxMSTID_H;
    }PSxMSTID[32];            /* 0x0300 */
    struct
    {
        uint32 PPSxMSTID_L;
        uint32 PPSxMSTID_H;
    }PPSxMSTID[8];            /* 0x0400 */
    struct
    {
        uint32 PPSExMSTID_L;
        uint32 PPSExMSTID_H;
    }PPSExMSTID[32];            /* 0x0440 */
    uint32 PCSxMSTID[32];    /* 0x0540 */
    uint32 PPCSxMSTID[8];    /* 0x05C0 */
} pcrBASE_t;

/** @def pcrREG1
*   @brief Pcr1 Register Frame Pointer
*
*   This pointer is used by the system driver to access the Pcr1 registers.
*/
#define pcrREG1 ((pcrBASE_t *)0xFFFF1000U)

/** @def pcrREG2
*   @brief Pcr2 Register Frame Pointer
*
*   This pointer is used by the system driver to access the Pcr2 registers.
*/
#define pcrREG2 ((pcrBASE_t *)0xFCFF1000U)

/** @def pcrREG3
*   @brief Pcr3 Register Frame Pointer
*
*   This pointer is used by the system driver to access the Pcr3 registers.
*/
#define pcrREG3 ((pcrBASE_t *)0xFFF78000U)

#define LPO_TRIM_VALUE       (((*(volatile uint32 *)0xF00801B4U) & 0xFFFF0000U)>>16U)
#define FSM_WR_ENA_HL       (*(volatile uint32 *)0xFFF87288U)
#define EEPROM_CONFIG_HL    (*(volatile uint32 *)0xFFF872B8U)
#define SYS_EXCEPTION        (*(volatile uint32 *)0xFFFFFFE4U)


/* Configuration registers initial value */
#define SYS_SYSPC1_CONFIGVALUE  0U

#define SYS_SYSPC2_CONFIGVALUE  1U

#define SYS_SYSPC7_CONFIGVALUE  0U

#define SYS_SYSPC8_CONFIGVALUE  0U

#define SYS_SYSPC9_CONFIGVALUE  1U

#define SYS_CSDIS_CONFIGVALUE   ( 0x00000000U\
                                | 0x00000000U \
                                | 0x00000008U \
                                | 0x00000080U \
                                | 0x00000000U \
                                | 0x00000000U \
                                | 0x00000000U\
                                | 0x4U )

#define SYS_CDDIS_CONFIGVALUE   ( (uint32)((uint32)0U << 4U )\
                                | (uint32)((uint32)1U << 5U )\
                                | (uint32)((uint32)0U << 8U )\
                                | (uint32)((uint32)0U << 9U )\
                                | (uint32)((uint32)0U << 10U)\
                                | (uint32)((uint32)0U << 11U) )

#define SYS_GHVSRC_CONFIGVALUE  ( (uint32)((uint32)SYS_PLL1 << 24U) \
                                | (uint32)((uint32)SYS_PLL1 << 16U) \
                                | (uint32)((uint32)SYS_PLL1 << 0U) )

#define SYS_VCLKASRC_CONFIGVALUE    ( (uint32)((uint32)SYS_VCLK << 8U)\
                                    | (uint32)((uint32)SYS_VCLK << 0U) )

#define SYS_RCLKSRC_CONFIGVALUE     ( (uint32)((uint32)1U << 24U)\
                                    | (uint32)((uint32)SYS_VCLK << 16U)\
                                    | (uint32)((uint32)1U << 8U)\
                                    | (uint32)((uint32)SYS_VCLK << 0U) )

#define SYS_MSTGCR_CONFIGVALUE      0x00000105U

#define SYS_MINITGCR_CONFIGVALUE    0x5U

#define SYS_MSINENA_CONFIGVALUE     0U

#define SYS_PLLCTL1_CONFIGVALUE_1   ( (uint32)0x00000000U \
                                    | (uint32)0x20000000U \
                                    | (uint32)((uint32)0x1FU << 24U) \
                                    | (uint32)0x00000000U \
                                    | (uint32)((uint32)(8U - 1U)<< 16U)\
                                    | (uint32)(0x9500U))

#define SYS_PLLCTL1_CONFIGVALUE_2   (((SYS_PLLCTL1_CONFIGVALUE_1) & 0xE0FFFFFFU) | (uint32)((uint32)(1U - 1U) << 24U))

#define SYS_PLLCTL2_CONFIGVALUE     ( (uint32)0x00000000U\
                                    | (uint32)((uint32)255U << 22U)\
                                    | (uint32)((uint32)7U << 12U)\
                                    | (uint32)((uint32)(1U - 1U)<< 9U)\
                                    | (uint32)61U)

#define SYS_SYSPC10_CONFIGVALUE     0U

#define SYS_LPOMONCTL_CONFIGVALUE_1 ((uint32)((uint32)1U << 24U) | LPO_TRIM_VALUE)
#define SYS_LPOMONCTL_CONFIGVALUE_2 ((uint32)((uint32)1U << 24U) | (uint32)((uint32)16U << 8U) | 16U)

#define SYS_CLKTEST_CONFIGVALUE     0x000A0000U

#define SYS_DFTCTRLREG1_CONFIGVALUE 0x00002205U

#define SYS_DFTCTRLREG2_CONFIGVALUE 0x5U

#define SYS_GPREG1_CONFIGVALUE  0x0005FFFFU

#define SYS_RAMGCR_CONFIGVALUE  0x00050000U

#define SYS_BMMCR1_CONFIGVALUE  0xAU

#define SYS_CLKCNTL_CONFIGVALUE     ( 0x00000100U \
                                    | (uint32)((uint32)1U << 16U) \
                                    | (uint32)((uint32)1U << 24U) )

#define SYS_ECPCNTL_CONFIGVALUE     ( (uint32)((uint32)0U << 24U)\
                                    | (uint32)((uint32)0U << 23U)\
                                    | (uint32)((uint32)(8U - 1U) & 0xFFFFU) )

#define SYS_DEVCR1_CONFIGVALUE  0xAU

#define SYS_SYSECR_CONFIGVALUE  0x00004000U
#define SYS2_PLLCTL3_CONFIGVALUE_1  ( (uint32)((uint32)(1U - 1U) << 29U)\
                                    | (uint32)((uint32)0x1FU << 24U) \
                                    | (uint32)((uint32)(8U - 1U)<< 16U) \
                                    | (uint32)(0x9500U))

#define SYS2_PLLCTL3_CONFIGVALUE_2  (((SYS2_PLLCTL3_CONFIGVALUE_1) & 0xE0FFFFFFU) | (uint32)((uint32)(1U - 1U) << 24U))
#define SYS2_STCCLKDIV_CONFIGVALUE  0U
#define SYS2_ECPCNTL1_CONFIGVALUE   0x50000000U
#define SYS2_CLK2CNTRL_CONFIGVALUE  (1U | 0x00000100U)
#define SYS2_HCLKCNTL_CONFIGVALUE    1U
#define SYS2_VCLKACON1_CONFIGVALUE  ( (uint32)((uint32)1U << 24U) \
                                    | (uint32)((uint32)1U << 20U) \
                                    | (uint32)((uint32)SYS_VCLK << 16U)\
                                    | (uint32)((uint32)1U << 8U)\
                                    | (uint32)((uint32)1U << 4U) \
                                    | (uint32)((uint32)SYS_VCLK << 0U) )
#define SYS2_CLKSLIP_CONFIGVALUE    0x5U
#define SYS2_EFC_CTLEN_CONFIGVALUE  0x5U


/** @fn void _gotoCPUIdle_(void)
*   @brief Take CPU to Idle state
*/
void _gotoCPUIdle_(void);

#endif /* INCLUDE_SYSTEM_H_ */
