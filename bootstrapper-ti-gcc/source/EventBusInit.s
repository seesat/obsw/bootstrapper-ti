 //
 // EventBusInit.s
 //
 //     Project: bootstrapper-ti-gcc
 //  Created on: 24.11.2023
 //      Author: Pascal Fiedler
 //    Function: Eveent Bus Initalization
 //
 	.section .text
    .syntax unified
    .cpu cortex-r5
    .arm

	.weak _coreEnableEventBusExport_
    .type _coreEnableEventBusExport_, %function

_coreEnableEventBusExport_:

        mrc   p15, #0x00, r0,         c9, c12, #0x00
        orr   r0,  r0,    #0x10
        mcr   p15, #0x00, r0,         c9, c12, #0x00
        bx    lr


