/*
 * CRC32Highlevel.c
 *
 *     Project: bootstrapper-ti-gcc
 *  Created on: 03.12.2023
 *      Author: Pascal Fiedler
 *    Function: Provide functionality to calculate CRC32
 *              checksum over a provided memory region
 */

/*!
 * \brief Function to calculate simple and fast the CRC32 checksum over a provided memory region (For further Information consult Studienarbeit from Pascal Fiedler TSL21)
 * \param data Pointer to the start address of the memory region which should be checked with CRC32
 * \param len  Length of the memory region
 * \return CRC32 checksum of the provided memory region
 */
unsigned int CRC32Calculation(const void* data, unsigned int len)
{
    unsigned char* buffer = (unsigned char*)data;
    unsigned int  crc = 0xFFFFFFFF;
    unsigned int  POLY = 0xEDB88320;
    unsigned int bit = 0;
    unsigned int length = len;

    while (length--)
    {
        crc = crc ^ *buffer++;
        bit = 0;
        while(bit < 8){
            bit++;
            if (crc & 1){
                crc = (crc >> 1) ^ POLY;
            }
            else{
                crc = (crc >> 1);
            }
        }
    }
    return ~crc;
}
