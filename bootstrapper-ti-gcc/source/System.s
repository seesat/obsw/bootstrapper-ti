 //
 // System.s
 //
 //     Project: bootstrapper-ti-gcc
 //  Created on: 25.11.2023
 //      Author: Pascal Fiedler
 //    Function: Definiton of the CPU idle state
 //

//-------------------------------------------------------------------------------
// Take CPU to IDLE state
// SourceId : CORE_SourceId_004
// DesignId : CORE_DesignId_004
// Requirements: HL_CONQ_CORE_SR12

@ Take CPU to IDLE state

	.section .text
    .syntax unified
    .cpu cortex-r5
    .arm

    .weak _gotoCPUIdle_
    .type _gotoCPUIdle_, %function

_gotoCPUIdle_:

        WFI
        nop
        nop
        nop
        nop
        bx    lr


