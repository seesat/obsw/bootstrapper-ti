/*
 * intvecs.s
 * 
 *     Project: bootstrapper-ti-gcc
 *  Created on: 11.02.2024
 *      Author: Pascal Fiedler
 */



	.syntax unified
	.cpu cortex-r4
	.arm

  .section .intvecs,"a",%progbits


	.extern Reset_Handler
	.extern Undefined_Handler
	.extern SVC_Handler
	.extern Prefetch_Handler
	.extern Data_Handler
	.extern IRQ_Handler

B Reset_Handler
B Undefined_Handler
B SVC_Handler
B Prefetch_Handler
B Data_Handler
NOP // Reserved vector
B IRQ_Handler
