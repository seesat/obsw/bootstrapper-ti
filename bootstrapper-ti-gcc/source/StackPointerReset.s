 //
 // StackPointerReset.s
 //
 //     Project: bootstrapper-ti-gcc
 //  Created on: 23.11.2023
 //      Author: Pascal Fiedler
 //    Function: Reset of the various stack pointers
 //

	@ Initialize Stack Pointers

	.section .text
    .syntax unified
    .cpu cortex-r5
    .arm

    .weak _coreInitStackPointer_
    .type _coreInitStackPointer_, %function

_coreInitStackPointer_:

        cps   #17
        ldr   sp,       fiqSp
        cps   #18
        ldr   sp,       irqSp
        cps   #19
        ldr   sp,       svcSp
        cps   #23
        ldr   sp,       abortSp
        cps   #27
        ldr   sp,       undefSp
        cps   #31
        ldr   sp,       userSp
        bx    lr

userSp:  .word 0x08000000+0x00001000
svcSp:   .word 0x08000000+0x00001000+0x00000100
fiqSp:   .word 0x08000000+0x00001000+0x00000100+0x00000100
irqSp:   .word 0x08000000+0x00001000+0x00000100+0x00000100+0x00000100
abortSp: .word 0x08000000+0x00001000+0x00000100+0x00000100+0x00000100+0x00000100
undefSp: .word 0x08000000+0x00001000+0x00000100+0x00000100+0x00000100+0x00000100+0x00000100
