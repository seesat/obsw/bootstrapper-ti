 /*
 ; StartUp.s
 ;
 ;     Project: bootstrapper-ti-gcc
 ;  Created on: 31.10.2023
 ;      Author: Pascal Fiedler
 ;    Function: TODO
 */


	.global _coreInitRegisters_
	.global _coreInitStackPointer_
	.global _memInit_
	.global _errata_SSWF021_45_both_plls
	.global _coreEnableEventBusExport_
	.global systemInit
	.global CRC32Calculation
	.extern _textstart
	.extern _textend


	.global Reset_Handler
	.global Undefined_Handler
	.global SVC_Handler
	.global Prefetch_Handler
	.global Data_Handler
	.global IRQ_Handler


.section .text

Reset_Handler:

	BL _coreInitRegisters_
	BL _coreInitStackPointer_
	BL _memInit_
	BL _errata_SSWF021_45_both_plls
	BL _coreEnableEventBusExport_
	BL systemInit



	//Load length of the image into register R1
	LDR r0, =BootloaderSize1
	LDR r1, [r0]
	LDR r1, [r1]


	//Load start address of the image into register R0
	//And add 4 to it bc start address holds the precalc. CRC
	LDR r0, =BootloaderStart1
	LDR r0, [r0]
	ADD r0, r0, #4

	//Call CRC32 claulation C-function
	//r0(start address) and r1(image size) hold the calling arguments
	BL CRC32Calculation

	//r0 containes the return value of the function call
	//load the pre calculated CRC value and compare it with r0
	LDR r1, =BootloaderStart1
	LDR r1, [r1]
	LDR r1, [r1]
	CMP r0, r1
	BEQ CRC1Valid

	//If not equal CRC Value gets reseted to zero only needed once
	LDR r0, =BootloaderCRCResult
	LDR r0, [r0]
	MOV r1, #0x0000
	STR r1, [r0]
	B	CRC2Check

	//Store the Result of the check in the memory
CRC1Valid:
	LDR r0, =BootloaderCRCResult
	LDR r0, [r0]
	MOV r1, #0x0001
	STR r1, [r0]

	//Perform the same operation on the second image
CRC2Check:
	LDR r0, =BootloaderSize2
	LDR r1, [r0]
	LDR r1, [r1]
	LDR r0, =BootloaderStart2
	LDR r0, [r0]
	ADD r0, r0, #4
	BL CRC32Calculation

	LDR r1, =BootloaderStart2
	LDR r1, [r1]
	LDR r1, [r1]
	CMP r0, r1
	BEQ CRC2Valid

	B	CRC3Check

CRC2Valid:
	LDR r0, =BootloaderCRCResult
	LDR r0, [r0]
	MOV r1, #0x0010
	LDR r2, [r0]
	ADD r1, r1, r2
	STR r1, [r0]

	//Perform the same operation on the third image
CRC3Check:
	LDR r0, =BootloaderSize3
	LDR r1, [r0]
	LDR r1, [r1]
	LDR r0, =BootloaderStart3
	LDR r0, [r0]
	ADD r0, r0, #4
	BL CRC32Calculation

	LDR r1, =BootloaderStart3
	LDR r1, [r1]
	LDR r1, [r1]
	CMP r0, r1
	BEQ CRC3Valid

	B	LoadBootloader

CRC3Valid:
	LDR r0, =BootloaderCRCResult
	LDR r0, [r0]
	MOV r1, #0x0100
	LDR r2, [r0]
	ADD r1, r1, r2
	STR r1, [r0]

	//Decide which bootloader image to start
LoadBootloader:
	LDR r0, =BootloaderCRCResult
	LDR r0, [r0]
	LDR r0, [r0]
	AND r1, r0, #0x0001
	CMP r1, #0x0001
	BNE StartImage2
	LDR r0, =BootloaderEntryPoint1
	LDR r0, [r0]
	BX r0

StartImage2:
	AND r1, r0, #0x0010
	CMP r1, #0x0010
	BNE StartImage2
	LDR r0, =BootloaderEntryPoint2
	LDR r0, [r0]
	BX r0

StartImage3:
	AND r1, r0, #0x0100
	CMP r1, #0x0100
	BNE Emergency
	LDR r0, =BootloaderEntryPoint3
	LDR r0, [r0]
	BX r0

	//Emergency State incase of no corret bootloader image
Emergency:
	B	Emergency


//Interrupt Handler Definitions
Undefined_Handler:
	NOP

SVC_Handler:
	NOP

Prefetch_Handler:
	NOP

Data_Handler:
	NOP

IRQ_Handler:
	NOP

.section .data

BootloaderCRCResult:	.word	0x08001500
BootloaderStart1: 		.word  	0x00200000
BootloaderSize1:		.word  	0x00200004
BootloaderEntryPoint1:	.word 	0x00203AF8

BootloaderStart2: 		.word  	0x00220000
BootloaderSize2:		.word  	0x00220004
BootloaderEntryPoint2:	.word 	0x00223AF8

BootloaderStart3: 		.word  	0x00240000
BootloaderSize3:		.word  	0x00240004
BootloaderEntryPoint3:	.word 	0x00240AF8
