import zlib
import sys

def calculate_crc32(file_path, start_address, end_address):
    # Open the file in binary mode
    with open(file_path, 'rb') as file:
        # Set the read pointer to the start address
        file.seek(start_address)
        # Read the data from the specified area
        data = file.read(end_address - start_address)
        # Calculate the CRC32 checksum of the data
        crc32_checksum = zlib.crc32(data)
        return crc32_checksum

def write_value_to_address(file_path, address, value):
    # Open the file in binary mode for reading and writing ('r+b')
    with open(file_path, 'r+b') as file:
        # Set the read/write pointer to the desired address
        file.seek(address)
        # Convert the value into the corresponding byte array
        value_bytes = value.to_bytes((value.bit_length() + 7) // 8, byteorder='little')
        # Write the value to the file
        file.write(value_bytes)


  

if __name__ == "__main__":
    file_path = sys.argv[1]                         # Path to the binary file
    start_address = int(sys.argv[2],16)             # Start address of the address range
    end_address = int(sys.argv[3],16) + 4           # Fisrt Unwritten Address
    crc_address = start_address                     # Memory address of the CRC checksum
    image_size = (end_address) - (start_address + 4)    # Image size (Fisrt Unwritten Address - start address for crc)
    size_address = start_address + 4                # Memory address of the Image size
    
    write_value_to_address(file_path,size_address,image_size)
    print(hex(start_address))
    crc_value = calculate_crc32(file_path, start_address + 4, end_address)
    print(hex(crc_value))
    write_value_to_address(file_path,crc_address,crc_value)
    